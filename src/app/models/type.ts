export enum PkmnType {
    normal,
    fighting,
    flying,
    poison,
    ground,
    rock,
    bug,
    ghost,
    steel,
    fire,
    water,
    grass,
    electric,
    psychic,
    ice,
    dragon,
    dark,
    fairy
}

export interface TypeReadout {
    typeName: string;
    id: number;
    numberWeakToType: number;
    affectedTeammates: number[];
    numberStrongToType: number;
    advantagedTeammates: Set<number>;
}

export interface HoverState {
    affectedType: number;
    isWeakness: boolean;
}

export interface HighlightStatus {
    isHighlighted: boolean;
    slot: number;
  }

// typechart from Bulbapedia - transposed for ease-of-access
// https://bulbapedia.bulbagarden.net/wiki/Type
//
// index 1 = defending pokemon type (in number format)
// index 2 = attack's type (in number format)
//
// example:
//  Thundershock (electric) is used on a snorlax (normal)
//  how effective is it?
//
//  electric = 12
//  normal = 0
//
//  defending_type_chart[0][12] => 1 (it does regular damage)
export const defending_type_chart: number[][] = [
    [1,2,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1],
    [1, 1, 2, 1, 1, 0.5, 0.5, 1, 1, 1, 1, 1, 1, 2, 1, 1, 0.5, 2],
    [1, 0.5, 1, 1, 0, 2, 0.5, 1, 1, 1, 1, 0.5, 2, 1, 2, 1, 1, 1],
    [1, 0.5, 1, 0.5, 2, 1, 0.5, 1, 1, 1, 1, 0.5, 1, 2, 1, 1, 1, 0.5],         // poison
    [1, 1, 1, 0.5, 1, 0.5, 1, 1, 1, 1, 2, 2, 0, 1, 2, 1, 1, 1],
    [0.5, 2, 0.5, 0.5, 2, 1, 1, 1, 2, 0.5, 2, 2, 1, 1, 1, 1, 1, 1],
    [1, 0.5, 2, 1, 0.5, 2, 1, 1, 1, 2, 1, 0.5, 1, 1, 1, 1, 1, 1],
    [0, 0, 1, 0.5, 1, 1, 0.5, 2, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1],
    [0.5, 2, 0.5, 0, 2, 0.5, 0.5, 1, 0.5, 2, 1, 0.5, 1, 0.5, 0.5, 0.5, 1, 0.5],
    [1, 1, 1, 1, 2, 2, 0.5, 1, 0.5, 0.5, 2, 0.5, 1, 1, 0.5, 1, 1, 0.5],
    [1, 1, 1, 1, 1, 1, 1, 1, 0.5, 0.5, 0.5, 2, 2, 1, 0.5, 1, 1, 1],           
    [1, 1, 2, 2, 0.5, 1, 2, 1, 1, 2, 0.5, 0.5, 0.5, 1, 2, 1, 1, 1],         // grass
    [1, 1, 0.5, 1, 2, 1, 1, 1, 0.5, 1, 1, 1, 0.5, 1, 1, 1, 1, 1],
    [1, 0.5, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 0.5, 1, 1, 2, 1],
    [1, 2, 1, 1, 1, 2, 1, 1, 2, 2, 1, 1, 1, 1, 0.5, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 0.5, 0.5, 0.5, 0.5, 1, 2, 2, 1, 2],
    [1, 2, 1, 1, 1, 1, 2, 0.5, 1, 1, 1, 1, 1, 0, 1, 1, 0.5, 2],
    [1, 0.5, 1, 2, 1, 1, 0.5, 1, 2 , 1 , 1 , 1 , 1 , 1 , 1 , 0 , 0.5, 1]
];

// type chart un-transposed
// used to rapidly look-up
//  type advantages given a
//  provided attack type
export const attacking_type_chart: number[][] = [
    [1,1,1,1,1,0.5,1,0,0.5,1,1,1,1,1,1,1,1,1],
    [2,1,0.5,0.5,1,2,0.5,0,2,1,1,1,1,0.5,2,1,2,0.5],
    [1,2,1,1,1,0.5,2,1,0.5,1,1,2,0.5,1,1,1,1,1],
    [1,1,1,0.5,0.5,0.5,1,0.5,0,1,1,2,1,1,1,1,1,2],
    [1,1,0,2,1,2,0.5,1,2,2,1,0.5,2,1,1,1,1,1],
    [1,0.5,2,1,0.5,1,2,1,0.5,2,1,1,1,1,2,1,1,1],
    [1,0.5,0.5,0.5,1,1,1,0.5,0.5,0.5,1,2,1,2,1,1,2,0.5],
    [0,1,1,1,1,1,1,2,1,1,1,1,1,2,1,1,0.5,1],
    [1,1,1,1,1,2,1,1,0.5,0.5,0.5,1,0.5,1,2,1,1,2],
    [1,1,1,1,1,0.5,2,1,2,0.5,0.5,2,1,1,2,0.5,1,1],
    [1,1,1,1,2,2,1,1,1,2,0.5,0.5,1,1,1,0.5,1,1],
    [1,1,0.5,0.5,2,2,0.5,1,0.5,0.5,2,0.5,1,1,1,0.5,1,1],
    [1,1,2,1,0,1,1,1,1,1,2,0.5,0.5,1,1,0.5,1,1],
    [1,2,1,2,1,1,1,1,0.5,1,1,1,1,0.5,1,1,0,1],
    [1,1,2,1,2,1,1,1,0.5,0.5,0.5,2,1,1,0.5,2,1,1],
    [1,1,1,1,1,1,1,1,0.5,1,1,1,1,1,1,2,1,0],
    [1,0.5,1,1,1,1,1,2,1,1,1,1,1,2,1,1,0.5,0.5],
    [1,2,1,0.5,1,1,1,1,0.5,0.5,1,1,1,1,1,2,2,1]
];