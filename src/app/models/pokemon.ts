export interface NamedApiResource {
    name: string;
    url: string;
}

export interface Pokemon {
    id: number;
    name: string;
    spriteUrl: string;
    moves: NamedApiResource[];
    types: string[];
}

export interface PokemonName {
    id: number;
    name: string;
}

export interface PkmnMoveType {
    id: number;
    name: string;
    type: string;
    damageClass: string;
}