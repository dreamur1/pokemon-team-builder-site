import { TestBed } from '@angular/core/testing';

import { PokemonDataService } from './pokemon-data.service';
import { HttpClientModule } from '@angular/common/http';

describe('PokemonDataService', () => {
  let service: PokemonDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({ imports:[HttpClientModule,  ] });
    service = TestBed.inject(PokemonDataService);
    service.base_url = "http://api.checkoutmypokemon.party";
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('fetchAllPokemonNames() should succeed', async () => {
    expect(service.pokemon_names.length).toBe(0);
    await service.fetchAllPokemonNames();
    expect(service.pokemon_names.length).toBeGreaterThan(1200);
  });

  it('fetchSinglePokemonData() should succeed', async () => {
    let expectedDitto = await service.fetchSinglePokemonData('ditto', 1);
    expect(expectedDitto.name).toBe('ditto');
    expect(expectedDitto.id).toBe(132);
    expect(expectedDitto.spriteUrl).toBe('https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/132.png');
    expect(expectedDitto.moves[0].name).toBe('transform');
    expect(expectedDitto.types[0]).toBe('normal');
  });

  it('fetchSingleMoveData() should succeed', async () => {
    await service.fetchSinglePokemonData('ditto', 0);
    let expectedTransform = await service.fetchSingleMoveData(['transform'], 0, 0);

    expect(expectedTransform.name).toBe('transform');
    expect(expectedTransform.id).toBe(144);
    expect(expectedTransform.damageClass).toBe('status');
    expect(expectedTransform.type).toBe('normal');
  });
});
