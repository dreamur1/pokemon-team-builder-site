import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ReplaySubject, firstValueFrom } from 'rxjs';
import { PkmnMoveType, Pokemon, PokemonName } from '../models/pokemon';
import { PkmnType, TypeReadout, attacking_type_chart, defending_type_chart } from '../models/type';
import { environment } from 'src/environments/environment';

interface PageResponse<T> {
  results: T[];
  count: number;
  next: string;
  previous: string;
}

interface LocalStorageData {
  slot: number;
  pokemonName: string;
  moves: string[];
}

@Injectable({
  providedIn: 'root'
})
export class PokemonDataService {
  type_readouts: TypeReadout[] = [];
  public type_readouts$ = new ReplaySubject<TypeReadout[]>(1);
  public weakness_hover_state$ = new ReplaySubject<number>(1);
  public strength_hover_state$ = new ReplaySubject<number[]>(1);

  selectedPokemonInfo: LocalStorageData[] = new Array<LocalStorageData>(6);

  pokemon_names: PokemonName[] = [];
  pokemon_data_buffer: Pokemon[] = [];
  move_data_buffer: PkmnMoveType[] = [];
  current_move_types: string[][] = new Array<string[]>(6);
  base_url: string = environment.apiUrl;

  constructor(private httpClient: HttpClient) { 
    let typeValues = Object.values(PkmnType)
    
    for(let i = 0; i < typeValues.length / 2; i++) {
      this.type_readouts.push({
        typeName: typeValues[i] as string,
        id: typeValues[i + 18] as number,
        numberWeakToType: 0,
        affectedTeammates: [],
        numberStrongToType: 0,
        advantagedTeammates: new Set<number>()
      } as TypeReadout);
    }

    for(let i = 0; i < 6; i++) {
      this.current_move_types[i] = new Array<string>(4).fill("");

      this.selectedPokemonInfo[i] = {
        slot: i,
        pokemonName: '',
        moves: []
      };
    }

    let stringifiedTeamData = localStorage.getItem("pokemon-team-data");

    if (stringifiedTeamData !== null) {
      this.selectedPokemonInfo = JSON.parse(stringifiedTeamData) as LocalStorageData[];
    }
  }

  public updateWeaknessHoverState(typeName: string): void {
    let nextValue: number = PkmnType[typeName as keyof typeof PkmnType] || -1;
    this.weakness_hover_state$.next(nextValue);
  }

  public updateStrengthHoverState(typeName: string): void {
    let typeNum: number = Object.values(PkmnType).findIndex(t => t === typeName);
    if (typeNum < 0) {
      this.strength_hover_state$.next([]);
      return;
    }
    let typeMatchupChart = defending_type_chart[typeNum];
    let typeWeaknessess: number[] = [];
    typeMatchupChart.forEach((t: number, i: number) => {
      if (t === 2) { typeWeaknessess.push(i); }
    });
    this.strength_hover_state$.next(typeWeaknessess);
  }

  async fetchAllPokemonNames(): Promise<void> {
    if (this.pokemon_names.length > 0) return;
    this.pokemon_names = await firstValueFrom(this.httpClient.get<PokemonName[]>(`${this.base_url}/pokemon`,  { headers: this.buildHeaders() }));
  }

  async fetchSinglePokemonData(pokemonName: string, pokemonSlot: number): Promise<Pokemon> {
    let cachedPokemon = this.pokemon_data_buffer.find(p => p.name === pokemonName);

    if (cachedPokemon) { 
      this.updateWeaknessInfoForTypeReadouts(cachedPokemon, pokemonSlot);
      this.storePokemonName(pokemonName, pokemonSlot);
      return cachedPokemon;
    }

    let newPokemonData = await firstValueFrom(this.httpClient.get<Pokemon>(`${this.base_url}/pokemon/${pokemonName}`, { headers: this.buildHeaders() }));
    this.pokemon_data_buffer.push(newPokemonData);
    this.updateWeaknessInfoForTypeReadouts(newPokemonData, pokemonSlot);
    this.storePokemonName(pokemonName, pokemonSlot);
    return this.pokemon_data_buffer[this.pokemon_data_buffer.length-1];
  }

  async fetchSingleMoveData(attacks: string[], pokemonSlot: number, attackSlot: number): Promise<PkmnMoveType> {
    this.clearStrengthInfoForSelectedPokemon(pokemonSlot);
    let cachedMove = this.move_data_buffer.find(m => m.name === attacks[attackSlot]);

    if (cachedMove) {
      this.updateStrengthInfoForTypeReadouts(cachedMove, pokemonSlot, attackSlot);
      this.storeMoveName(attacks[attackSlot], pokemonSlot, attackSlot);
      return cachedMove;
    }

    let newMoveData = await firstValueFrom(this.httpClient.get<PkmnMoveType>(`${this.base_url}/move/${attacks[attackSlot]}`, { headers: this.buildHeaders() }));
    this.move_data_buffer.push(newMoveData);
    this.updateStrengthInfoForTypeReadouts(newMoveData, pokemonSlot, attackSlot);
    this.storeMoveName(attacks[attackSlot], pokemonSlot, attackSlot);
    return newMoveData;
  }

  public clearStoredPokemonMoveData(pokemonSlot: number): void {
    this.current_move_types[pokemonSlot] = new Array<string>(4).fill("");
    this.clearStrengthInfoForSelectedPokemon(pokemonSlot);
    this.type_readouts$.next(this.type_readouts);
  }

  private storePokemonName(pokemonName: string, slot: number): void {
    if (this.selectedPokemonInfo[slot].pokemonName !== pokemonName) {
      this.selectedPokemonInfo[slot].pokemonName = pokemonName;
      this.selectedPokemonInfo[slot].moves = new Array<string>(4).fill('');
      localStorage.setItem("pokemon-team-data", JSON.stringify(this.selectedPokemonInfo));
    }
  }

  private storeMoveName(moveName: string, pokemonSlot: number, moveSlot: number): void {
    this.selectedPokemonInfo[pokemonSlot].moves[moveSlot] = moveName;
    localStorage.setItem("pokemon-team-data", JSON.stringify(this.selectedPokemonInfo));
  }

  private buildHeaders(): HttpHeaders {
    let headers = new HttpHeaders();
    return headers.append('Access-Control-Allow-Origin', '*').append('Access-Control-Allow-Methods', 'GET, OPTIONS');
  }

  private clearStrengthInfoForSelectedPokemon(pokemonSlot: number): void {
    this.type_readouts.forEach((readout: TypeReadout) => {
      readout.advantagedTeammates = new Set([...readout.advantagedTeammates.values()].filter((val: number) => val !== pokemonSlot));
      readout.numberStrongToType = readout.advantagedTeammates.size;
    })
  }

  private updateStrengthInfoForTypeReadouts(selectedMove: PkmnMoveType, pokemonSlot: number, attackSlot: number): void {
    this.current_move_types[pokemonSlot][attackSlot] = selectedMove.type;

    // status moves don't do damage - we won't track 'em
    if (selectedMove.damageClass=== "status") { return; }
    
    this.current_move_types[pokemonSlot].forEach((attackType: string) => {
      if (!attackType || attackType.trim() === '') { return; }
      attacking_type_chart[PkmnType[attackType as keyof typeof PkmnType]].forEach((typeMatchup: number, i: number) => {
        if (typeMatchup > 1) {
          this.type_readouts[i].advantagedTeammates.add(pokemonSlot);
          this.type_readouts[i].numberStrongToType = this.type_readouts[i].advantagedTeammates.size;
        }
      })
    });

    this.type_readouts$.next(this.type_readouts);
  }

  private updateWeaknessInfoForTypeReadouts(selectedPokemon: Pokemon, pokemonSlot: number): void {
    let typeWeaknessess: number[][] = [];

    selectedPokemon.types.forEach((typeName: string) => {
      let defendingType: number = PkmnType[typeName as keyof typeof PkmnType];
      typeWeaknessess.push(defending_type_chart[defendingType]);
    });

    this.type_readouts.forEach((readout: TypeReadout) => {
      readout.affectedTeammates = readout.affectedTeammates.filter((teammateId: number) => teammateId !== pokemonSlot)
      readout.numberWeakToType = readout.affectedTeammates.length;
    });

    for(let attackTypeIndex = 0; attackTypeIndex < typeWeaknessess[0].length; attackTypeIndex++) {
      let foundIndex = this.type_readouts.findIndex((readout: TypeReadout) => readout.id === attackTypeIndex);

      // implies the selected pokemon has
      // two types we need to check against
      if (typeWeaknessess.length > 1 && (typeWeaknessess[0][attackTypeIndex] * typeWeaknessess[1][attackTypeIndex]) > 1) {
        this.type_readouts[foundIndex].numberWeakToType += 1;
        this.type_readouts[foundIndex].affectedTeammates.push(pokemonSlot);
      } else if (typeWeaknessess.length === 1 && typeWeaknessess[0][attackTypeIndex] > 1) {
        this.type_readouts[foundIndex].numberWeakToType += 1;
        this.type_readouts[foundIndex].affectedTeammates.push(pokemonSlot);
      }
    }

    this.type_readouts$.next(this.type_readouts);
  }
}
