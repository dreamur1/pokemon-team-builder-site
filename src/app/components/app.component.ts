import { Component } from '@angular/core';
import { PokemonDataService } from '../services/pokemon-data.service';
import { combineLatest, map } from 'rxjs';
import { HighlightStatus } from '../models/type';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  title = 'pokemon-team-builder-site';
  highlightStatus: HighlightStatus[] = [];
  selectedPokemonIcons: string[] = new Array<string>(6).fill("./assets/pokeball.png");
  isMobile: boolean = false;
  hasVScroll: boolean = false;

  constructor(private pokemonDataService: PokemonDataService) {
    this.isMobile = window.innerWidth <= 1024;

    this.checkIfHasVScroll();

    this.pokemonDataService.fetchAllPokemonNames();

    for (let i = 0; i < 6; i++) {
      this.highlightStatus.push({
        isHighlighted: false,
        slot: i
      });
    }

    combineLatest([this.pokemonDataService.weakness_hover_state$, this.pokemonDataService.type_readouts$]).pipe(
      map((result) => {
        this.highlightStatus.forEach(s => { s.isHighlighted = false; });

        let currentHoverState: number = result[0];
        let typeReadoutData = result[1];

        if (currentHoverState < 0) { return; }

        let pokemonIdsToHighlight: number[] = typeReadoutData[currentHoverState].affectedTeammates;
        pokemonIdsToHighlight.forEach((id: number) => this.highlightStatus[id].isHighlighted = true);
      })
    ).subscribe();

    if (this.isMobile) {
      const resizeObserver = new ResizeObserver(entries => {
        this.checkIfHasVScroll();
      });
      
      // start observing a DOM node
      resizeObserver.observe(document.body)
    }
  }

  checkIfHasVScroll(): void {
    this.hasVScroll = document.body.scrollHeight > window.innerHeight;
  }

  storePokemonIconForSlot(slot: number, iconLocation: string): void {
    this.selectedPokemonIcons[slot] = iconLocation || "./assets/pokeball.png";
    this.checkIfHasVScroll();
  }
}
