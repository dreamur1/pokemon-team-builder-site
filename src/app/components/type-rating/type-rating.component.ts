import { Component, Input } from '@angular/core';
import { map } from 'rxjs';
import { TypeReadout } from 'src/app/models/type';
import { PokemonDataService } from 'src/app/services/pokemon-data.service';

@Component({
  selector: 'type-rating',
  templateUrl: './type-rating.component.html',
})
export class TypeRatingComponent {
  displayedWeaknesses: TypeReadout[] = [];
  displayedStrengths: TypeReadout[] = [];

  @Input()
  isMobile: boolean = false;

  constructor(private pokemonDataService: PokemonDataService) {
    this.pokemonDataService.type_readouts$.pipe(
      map((readouts: TypeReadout[]) => {
        this.displayedWeaknesses = [];
        this.displayedWeaknesses = readouts.filter((r: TypeReadout) => r.numberWeakToType > 1)
                                           .sort((a: TypeReadout, b: TypeReadout) => b.numberWeakToType - a.numberWeakToType);
        this.displayedStrengths = [];
        this.displayedStrengths = readouts.filter((r: TypeReadout) => r.numberStrongToType > 1)
                                          .sort((a: TypeReadout, b: TypeReadout) => b.numberStrongToType - a.numberStrongToType);
      })
    ).subscribe();
  }

  public updateServiceHoverState(typeName: string, isWeakness: boolean = true): void {
    if (isWeakness) this.pokemonDataService.updateWeaknessHoverState(typeName);
    else this.pokemonDataService.updateStrengthHoverState(typeName);
  }

}
