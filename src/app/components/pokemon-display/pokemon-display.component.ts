import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { PokemonDataService } from 'src/app/services/pokemon-data.service';
import { FormControl } from '@angular/forms';
import { NamedApiResource, Pokemon } from 'src/app/models/pokemon';
import { map } from 'rxjs';
import { TypeReadout } from 'src/app/models/type';

@Component({
  selector: 'pokemon-display',
  templateUrl: './pokemon-display.component.html',
})
export class PokemonDisplayComponent implements OnInit {

  @ViewChild('input') input!: ElementRef<HTMLInputElement>;
  @Input() pokemonSlot: number = 0;
  @Input() isHighlighted: boolean = false;
  @Output() pokemonSelectedEvent = new EventEmitter<string>();

  pokemonControl = new FormControl('');
  filteredNames: string[] = [];
  selectedPokemon: Pokemon | undefined;
  weaknesses: TypeReadout[] = [];
  availableMoves: NamedApiResource[] = [];

  constructor(private pokemonDataService: PokemonDataService) {
    this.pokemonDataService.type_readouts$.pipe(
      map((readouts: TypeReadout[]) => {
        this.weaknesses = readouts.filter((r: TypeReadout) => r.affectedTeammates.includes(this.pokemonSlot))
      })
    ).subscribe();
  }

  ngOnInit(): void {
    let storedName = this.pokemonDataService.selectedPokemonInfo[this.pokemonSlot].pokemonName;
    if (storedName.trim()) {
      this.pokemonControl.setValue(storedName);
      this.selectPokemon()
    }
  }

  public runFilter(): void {
    const filterValue = this.input.nativeElement.value.toLowerCase();
    this.filteredNames = this.pokemonDataService.pokemon_names
                          .filter(option => option.name.toLowerCase().includes(filterValue))
                          .map(r => r.name);

    if (this.filteredNames.includes(this.pokemonControl.value as string) && this.pokemonControl.value !== this.selectedPokemon?.name) {
      this.selectPokemon();
    }
  }

  private selectPokemon(): void {
    this.pokemonDataService.fetchSinglePokemonData(this.pokemonControl.value as string, this.pokemonSlot).then((pkmn: Pokemon) => {
      this.selectedPokemon = pkmn;
      this.availableMoves = pkmn.moves.sort((a: NamedApiResource, b: NamedApiResource) => a.name < b.name ? -1 : 1);
      this.pokemonSelectedEvent.emit(pkmn.spriteUrl)
    });
  }

}
