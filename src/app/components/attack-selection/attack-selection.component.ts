import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { map } from 'rxjs';
import { NamedApiResource } from 'src/app/models/pokemon';
import { HighlightStatus, PkmnType } from 'src/app/models/type';
import { PokemonDataService } from 'src/app/services/pokemon-data.service';

@Component({
  selector: 'attack-selection',
  templateUrl: './attack-selection.component.html',
})
export class AttackSelectionComponent implements OnInit, OnChanges {
  @Input() availableMoves: NamedApiResource[] = [];
  @Input() pokemonSlot: number = 0;
  selectedMoves: string[] = new Array<string>(4).fill("");
  selectedMoveTypes: string[] = new Array<string>(4);
  highlightStatus: HighlightStatus[] = [];

  constructor(private pokemonDataService: PokemonDataService) {
    for (let i = 0; i < 4; i++) {
      this.highlightStatus.push({
        isHighlighted: false,
        slot: i
      });
    }

    pokemonDataService.strength_hover_state$.pipe(
      map((moveTypesToHighlight: number[]) => {
        this.highlightStatus.forEach(s => { s.isHighlighted = false; });
        let selectedMoveNums = this.selectedMoveTypes.map((t: string) => PkmnType[t as keyof typeof PkmnType]);
        selectedMoveNums.forEach((move: number, i: number) => {
          if (moveTypesToHighlight.includes(move)) {
            this.highlightStatus[i].isHighlighted = true;
          }
        });
      })
    ).subscribe();
  }

  async ngOnInit(): Promise<void> {
    let storedMoves = this.pokemonDataService.selectedPokemonInfo[this.pokemonSlot].moves;
    this.selectedMoves = storedMoves;

    let updateCallsToMake: Promise<void>[] = [];

    this.selectedMoves.forEach((moveName, index) => {
      if (moveName.trim()) {
        updateCallsToMake.push(this.updateSelection(index))
      }
    })
    await Promise.all(updateCallsToMake);
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes['availableMoves']) {
      this.selectedMoves = new Array<string>(4).fill("");
      this.selectedMoveTypes = new Array<string>(4);
      this.highlightStatus.forEach(s => { s.isHighlighted = false; });
      this.pokemonDataService.clearStoredPokemonMoveData(this.pokemonSlot);
    }
  }

  public async updateSelection(attackNumber: number): Promise<void> {
    if (!this.selectedMoves[attackNumber]) { 
      this.selectedMoveTypes[attackNumber] = "";
      return; 
    }
    let moveData = await this.pokemonDataService.fetchSingleMoveData(this.selectedMoves, this.pokemonSlot, attackNumber);
    this.selectedMoveTypes[attackNumber] = moveData.type;
  }
}
