# PokemonTeamBuilderSite

A quick-n-dirty single-page-application which will tell you how many shared type weaknesses you have on your team & how well your team's moves covers the different defending types.

A [C# backend](https://gitlab.com/dreamur1/pokemon-team-builder-service) is currently being worked on, to reduce the amount of calls this site would otherwise make to the public "PokeApi".

## Example

A list of all type weaknesses will be displayed at the top of the page. Hovering over any icon will display the name of the type and the number of team mates that are either weak to that type or who have a type-effective move against that type.

![Looping gif file of the hover effect](src/assets/hoverDemo.gif)


## Running it yourself

As with any other angular app, use `ng serve` or `npm start`. The site is configured to run on angular's default port of `4200`. 

## Credits / Dependencies

- [Angular CLI](https://github.com/angular/angular-cli) version 16.2.8
- [NodeJs](https://nodejs.org/en) version 20.9.0
- [PokeApi](https://pokeapi.co/) version 2
- [MaterialUi](https://material.angular.io/) version 16.2.11
