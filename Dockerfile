### BUILD THE SITE ###
FROM node as build
WORKDIR /usr/src/app
COPY package.json package-lock.json ./
RUN npm install
COPY . .
RUN npm run build -- --configuration production

### RUN THE SITE ###
FROM nginx
COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=build /usr/src/app/dist/pokemon-team-builder-site /usr/share/nginx/html